![modderbrothers](../assets/product.png)

# Manuel - ZX-81VG5000 - 32Ko de RAM - Bouton RESET

[English Version Here](./README.md)

## Outillage requit

Obligatoire pour l'installation du mod :
- Tournevis cruciforme
- Couteau sans dent ou equivalent en plastique dur
- Fer à souder + étain

Optionnel pour l'installation du reset externe / sélecteur on/off:
- Perceuse

## Installation du Mod

#### Précautions générales

- Toutes les manipulations doivent être effectuées hors tension.
- Vous êtes responsables de votre outillage et de ses manipulations. Si vous n'avez pas les aptitudes et/ou l'expérience requise, nous vous conseillons de faire réaliser ces manipulations par une personne qualifiée.

#### Précautions à prendre avant de toucher les cartes et les composants électroniques

Certains composants sont très sensibles à l'électricité statique.
Veillez à vous décharger avant l'ouverture de votre ZX-81 :
- Placez-vous dans une piece en carrelage, et évitez la moquette !
- Évitez les pulls en laine ou autres vêtements qui se chargent facilement !
- Touchez la carcasse métallique d'un appareil électrique pour vous décharger à la terre.

#### Ouverture du ZX-81

Retourner la machine, puis retirer les 5 vis comme indiqué sur la photo.

Décollez délicatement les pieds, et posez-les afin de ne pas endommager les surfaces collantes.

![dévissage](./assets/1.unscrew.png)

Retirer toutes les vis du boitier à l'aide d'un tournevis cruciforme, puis retirer la partie inférieure du boitier.

Retirez ensuite les 2 vis qui fixent la carte mère.

![ouverture](./assets/2.opened.png)

Les cartes mères des ZX-81 ne sont pas toutes de la même couleur, ne vous inquiétez pas si la vôtre est verte, beige, ou d'une tout autre couleur.

#### Retirer le clavier

Tournez délicatement la carte mère, sans forcer sur la nappe qui retient le clavier.

![carte mère](./assets/3.motherboard.png)

Une fois les composants visibles devant vous, sortez les 2 nappes clavier de leur logement. Retirez-les l'une après l'autre délicatement et à la verticale. Prenez-les des deux cotés du connecteur pour éviter une déchirure.

![Nappes clavier](./assets/4.remove.keyboard.png)

***Attention*** : certaines cartes mères de dernière génération ont le processeur directement soudé et non pas sur support !

Dans ce cas là, il faut prévoir de dessouder le processeur. Deux solutions s'offrent à vous :
- Dessouder proprement le processeur pour le réutiliser.
- Couper les pattes du processeur, dessouder proprement chaque patte, puis racheter un processeur Z80 recent et souder un support DIP 40 large. 

#### Retirer le processeur

Voici les différents éléments sur la carte mère :

![carte mère](./assets/5.motherboard.png)

Maintenant, il faut sortir le processeur de son support :
Pour ce faire, glissez ***très délicatement*** la lame du couteau entre le processeur et son support.
Puis faites légèrement tourner le couteau dans le sens des aiguilles d'une montre et dans le sens inverse, pour que la lame soulève le CPU d'un millimetre de chaque côté des pattes.
Répétez l'opération en alternance de chaque côté du support, jusqu'à ce que le chip sorte de lui-même.
Dans la mesure du possible, évitez de toucher les pattes métalliques du processeur.

_Faite attention à ne pas faire basculer la pointe du couteau sur la carte mère, cela pourrait endommager les pistes qui passent dessous !_

Une fois le processeur retiré, vous devriez avoir ceci sous les yeux :

![processeur retiré](./assets/6.remove.cpu.png)

#### Assembler le mod

Vous allez pouvoir enficher le mod dans le support du CPU.

Positionnez-le correctement, puis enfoncez-le le plus possible. S'il a tendance à sortir (selon les supports) ou s'il bouge, appuyez plus fort. Afin de ne pas risquer de casser, ni le mod, ni la carte mère, nous vous conseillons de poser la carte mère sur une surface souple telle qu'un tapis souris, un torchon ou même un carton. Cela vous permettra de force à l'insertion sans endommager les soudures ou la carte mère elle-même.

![mod inséré](./assets/7.insert.mod.png)

#### Remonter le processeur

Ensuite, remettez le processeur dans le mod.
Notez que vous pouvez faire cette operation avant de placer le mod sur la carte mère.
Faite ***très attention*** lors de l'insertion du processeur dans le support du Mod : Insérez les pattes d'un côté, puis aidez-vous de la lame du couteau pour pousser les pattes de l'autre côté, de façon à les aligner sur les trous du support si nécessaire.
Assurez-vous que toutes les pattes du processeur sont correctement en place avant d'appuyer dessus délicatement pour l'insérer complètement.

#### Souder ROMCS !

Sous ce nom barbare se cache le signal d'activation de la ROM. Par défaut la rom est active à plusieurs endroits dans l'espace mémoire de 64ko, risquant d'entrer en collision avec notre extension RAM. Heureusement, Sinclair à prévu de pouvoir surcharger ce signal afin qu'il n'interfère plus avec les extensions, et c'est ce que nous allons faire.

Prenez le cable qui part de la position **ROMCS** sous le mod (celui le plus à gauche), puis soudez son extrémité à la resistance R28.

Selon la carte mère, R28 peut être à deux positions différentes :
- Si la ROM est à gauche du CPU, R28 se trouve à gauche de la ROM, et il faudra souder notre fil à **droite** de la résistance.
- Si la ROM est à droite du CPU, R28 se trouve en bas à gauche du CPU et il faudra souder notre fil **en bas** de la résistance.

![Soudure R28](./assets/8.solder.romcs.png)

Une vue un peu plus précise :

![R28](./assets/9.solder.romcs.png)

#### Remettre le clavier

Poursuivez en remettant le clavier.
Pour ça, procédez exactement de la même façon que pour l'extraite, mais en sens inverse, en prenant soin de ne pas plier les nappes souples.

#### Remonter le boitier et tester!

Si vous n'avez pas à monter d'option, vous pouvez désormais refermer votre ZX-81, en procédant encore une fois, en sens inverse de son ouverture.
Commencez par refixer la carte mère sur le haut du boitier. Puis refermez le boitier en remettant les 5 vis extérieures.

C'est terminé ! Vous pouvez brancher et allumer votre ZX81 !

## Installation des options

#### Installation du bouton reset

Déterminez la position de votre futur bouton reset. Nous vous conseillons le côté droit, environ à l'opposé de la sortie antenne du démodulateur.

Faites attention à la carte ! Percez à 8mm le plus haut possible, sans risquer de casser le boitier bien entendu.

Ensuite, passez les 2 fils de l'extérieur vers l'intérieur du boitier. Passez l'écrou par le deux fils, puis vissez le bouton sur le boitier.

Une fois le bouton solidement maintenu, enficher les 2 cosses femelles dans le header male du reset sur le mod.

C'est terminé. Remontez votre ZX-81 et profitez d'un vrai bouton reset sans être obligé d'éteindre/allumer ou pire : brancher/débrancher comme dans les années 80 !
