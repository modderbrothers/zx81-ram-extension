![modderbrothers](../assets/product.png)

# Manual - ZX-81VG5000 - 32KB RAM - RESET Button

[Version Française ici](./LISEZMOI.md)

## Tools required

Required for mod installation:
- Screwdriver
- Toothless knife or equivalent
- Iron solder + soldering tin

Optional for the installation of the external reset:
- Drill

## Mod Installation

#### General precautions

- All manipulations must be carried out without power.
- You are responsible for your tools and their handling. If you do not have the required skills and/or experience, we advise you to have these manipulations done by a qualified person.

#### Precautions to take before touching the boards and electronic components

Some components are very sensitive to electrostatic discharge.
Be sure to discharge yourself before opening your VG5000:
- Place yourself in a tiled room, and avoid carpeting!
- Avoid wool sweaters or other clothes that charge easily!
- Touch the metal casing of an electrical appliance to discharge to ground.

#### Opening your ZX-81

Turn the machine over, then remove the 5 screws as shown in the photo.

Carefully peel off the feet, and place them down so as not to damage the sticky surfaces.

![Unscrew](./assets/1.unscrew.png)

Remove all the screws from the case with a Phillips screwdriver, then remove the bottom part of the case.

Then remove the 2 screws that fix the motherboard.

![Opening](./assets/2.opened.png)

Not all ZX-81 motherboards are the same color, so don't worry if yours is green, beige, or any other color.

#### Remove keyboard

Gently turn the motherboard, without forcing on the flat cables that holds the keyboard.

![Motherboard](./assets/3.motherboard.png)

Once the components are visible in front of you, take out the 2 keyboard flat cables from their connector. Remove them one after the other carefully and vertically. Take them from both sides of the connector to avoid tearing.

![Keyboard flat cable](./assets/4.remove.keyboard.png)

***Warning***: some last generation motherboards have the processor directly soldered and not socketed!

In this case, you must plan to desolder the processor. You have two solutions:
- Desolder the processor to reuse it.
- Cut the legs of the processor, desolder each leg cleanly, then buy a new Z80 processor and solder a large DIP 40 bracket.

#### Remove the processor

Here are the different elements on the motherboard:

![Motherboard](./assets/5.motherboard.png)

Now, you have to take the processor out of its support:
To do this, slide ***very gently*** the blade of the knife between the processor and its socket.
Then turn the knife slightly clockwise and counter-clockwise, so that the blade lifts the CPU by one millimeter on each side of the legs.
Repeat the operation alternately on each side of the socket, until the chip comes out by itself.
If possible, avoid touching the metal legs of the processor.

Be careful not to knock the knife on the motherboard, it could damage the tracks that run underneath!

Once the processor is removed, you should have this in front of you:

![Removed processor](./assets/6.remove.cpu.png)

#### Assembling the mod

Now you can plug the mod into the CPU socket.

Position it correctly, then push it in as hard as possible. If it tends to come out (depending on the bracket) or if it moves, press harder. In order not to risk breaking either the mod or the motherboard, we advise you to put the motherboard on a soft surface such as a mouse pad, a cloth or even a cardboard. This will allow you to force the insertion without damaging the soldering or the motherboard itself.

![Inserted mod](./assets/7.insert.mod.png)

#### Reassemble the processor

Then, put the processor back into the mod.
Note that you can do this operation before placing the mod on the motherboard.
Be ***very careful*** when inserting the processor into the mod socket: Insert the legs on one side, then use the knife blade to push the legs on the other side, so that they line up with the holes in the socket if necessary.
Make sure all the processor legs are properly seated before gently pressing the processor in completely.

#### Solder ROMCS!

Under this barbaric name hides the ROM activation signal. By default, the ROM is active in several areas in the 64kb memory space, risking collision with our RAM extension. Fortunately, Sinclair has planned to override this signal so that it does not interfere with the extensions, and this is what we will do.

Take the cable that runs from the **ROMCS** position under the mod (the leftmost one), then solder its end to the R28 resistor.

Depending on the motherboard, R28 can be in two different positions:
- If the ROM is on the left side of the CPU, R28 is on the left side of the ROM, and we will need to solder our wire to **right** resistor pin.
- If the ROM is on the right side of the CPU, R28 is on the bottom left of the CPU and we will have to solder our wire to **bottom** resistor pin.

![Solder R28](./assets/8.solder.romcs.png)

A slightly more accurate view:

![R28](./assets/9.solder.romcs.png)

#### Replace the keyboard

Continue by putting the keyboard back on.
To do this, proceed in exactly the same way as for the extracted one, but in the opposite direction, taking care not to bend the flexible cable.

#### Reassemble the case and test!

If you don't have to mount any options, you can now close your ZX-81, again in the opposite direction of the way it was opened.
Start by reattaching the motherboard to the top of the case. Then close the case by putting back the 5 external screws.

It is finished! You can plug in and turn on your ZX81!

## Installing options

#### Reset button installation

Determine the position of your future reset button. We recommend the right side, approximately opposite the antenna output of the demodulator.

Be careful with the board! Drill 8mm as high as possible, without breaking the case of course.

Then, pass the 2 wires from the outside to the inside of the box. Pass the nut through the two wires, then screw the button on the case.

Once the button is firmly held, plug the 2 female terminals into the male header of the reset on the mod.

You're done. Reassemble your ZX-81 and enjoy a real reset button without having to turn off/on or worse: plug/unplug like in the 80s!
